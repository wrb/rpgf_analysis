# create table
create table rpgf_posts (thread_title text, forum_name text, post_text text, post_time timestamp, top_forum_name text);

# set parameters
.separator ,
.header on

# import
.import rpgf_posts_with_topforum.csv rpgf_posts

# check data
select count(*) from rpgf_posts;